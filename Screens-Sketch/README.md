# Passenger App

### 1. Intro
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/1_passenger_intro.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/1_passenger_intro.jpg" width="1000" height="600" />
</center>
<br>

### 2. Splash
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/2_passenger_splash.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/2_passenger_splash.jpg" width="1000" height="600" />
</center>
<br>

### 3. Login
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/3_passenger_login.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/3_passenger_login.jpg" width="1000" height="600" />
</center>
<br>


### 4. Register
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/4_passenger_register.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/4_passenger_register.jpg" width="1000" height="600" />
</center>
<br>

### 5. Main Screen
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/5_passenger_mainscreen.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/5_passenger_mainscreen.jpg" width="1000" height="600" />
</center>
<br>

### 6. Ride Confirmation
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/6_passenger_ride_confirmation.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/6_passenger_ride_confirmation.jpg" width="1000" height="600" />
</center>
<br>


### 7. Ride Connection
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/7_passenger_ride_connection.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/7_passenger_ride_connection.jpg" width="1000" height="600" />
</center>
<br>


### 8. During Ride
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/8_passenger_during_ride.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/8_passenger_during_ride.jpg" width="1000" height="600" />
</center>
<br>


### 9. Ride Completion
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/9_passenger_ride_completion.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/9_passenger_ride_completion.jpg" width="1000" height="600" />
</center>
<br>


### 10. Navigation Drawer
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/10_passenger_navigation_drawer.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/passenger-app/10_passenger_navigation_drawer.jpg" width="1000" height="600" />
</center>
<br>
<br>


# Driver App

### 1. Splash
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/1_driver_splash.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/1_driver_splash.jpg" width="1000" height="600" />
</center>
<br>

### 2. Intro
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/2_driver_intro.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/2_driver_intro.jpg" width="1000" height="600" />
</center>
<br>

### 3. Login
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/3_driver_login.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/3_driver_login.jpg" width="1000" height="600" />
</center>
<br>

### 4. Join Request
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/4_driver_join_request.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/4_driver_join_request.jpg" width="1000" height="600" />
</center>
<br>

### 5. Main Screen
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/5_driver_main_screen.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/5_driver_main_screen.jpg" width="1000" height="600" />
</center>
<br>

### 6. Request For Ride
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/6_driver_request_for_ride.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/6_driver_request_for_ride.jpg" width="1000" height="600" />
</center>
<br>

### 7. Navigation To Pickup
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/7_driver_navigate_to_pickup.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/7_driver_navigate_to_pickup.jpg" width="1000" height="600" />
</center>
<br>

### 8. Start Ride
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/8_driver_start_ride.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/8_driver_start_ride.jpg" width="1000" height="600" />
</center>
<br>

### 9. Navigation To Drop
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/9_driver_navigate_to_drop.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/9_driver_navigate_to_drop.jpg" width="1000" height="600" />
</center>
<br>

### 10. Ride Details
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/10_driver_ride_details.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/10_driver_ride_details.jpg" width="1000" height="600" />
</center>
<br>

### 11. Navigation Drawer
<center>
<img src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/11_driver_navigation_drawer.jpg" data-canonical-src="https://gitlab.com/coopon/uauto/uauto-interaction-design/raw/master/Screens-Sketch/driver-app/11_driver_navigation_drawer.jpg" width="1000" height="600" />
</center>
<br>